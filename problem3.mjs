export function problem3(data) {

  let car_model_list = []
  let model = "car_model";

  for (let obj of data) {
    car_model_list.push(obj[model]);
  }

  let done = true

  while (done) {
    done = false
    for (let j = 1; j < car_model_list.length; j++) {
      if (car_model_list[j - 1] > car_model_list[j]) {
        done = true
        let max = car_model_list[j - 1]
        car_model_list[j - 1] = car_model_list[j]
        car_model_list[j] = max
      }
    }
  }

  return car_model_list
}
