
export function problem2(data){
  let id = "id";
  let company = "car_make";
  let model = "car_model";

  for(let obj of data){
    if(obj[id] === data.length){
        return `Last car is a ${obj[company]} ${obj[model]}`
    }
  }
}
