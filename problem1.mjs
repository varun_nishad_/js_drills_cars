
export function problem1(data){
  let id = "id";
  let company = "car_make";
  let model = "car_model";
  let year = "car_year"

  for(let obj of data){
    if(obj[id] === 33){
        return `Car 33 is a ${obj[year]} ${obj[company]} ${obj[model]}`
    }
  }
}
