
export function problem5(data){
  let years = []
  let year = "car_year"

  for(let obj of data){
    if(obj[year] < 2000){
      years.push(obj[year])
    }
  }

  return years
}

