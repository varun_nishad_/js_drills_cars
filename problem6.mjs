
export function problem6(data){
  let array = []
  let company = "car_make";

  for(let obj of data){
    if(obj[company] == 'Audi' || obj[company] == 'BMW'){
      array.push(obj)
    }
  }

  return array
}
